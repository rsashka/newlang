# История версий языка NewLang

## [Релиз 0.3 (07.11.2022) - текущая версия](https://github.com/rsashka/newlang/releases/tag/v0.3.0)
### Новые возможности и изменения в синтаксисе NewLang
- Простые чистые функции удалены.
- Зафиксирован синтаксис операторов проверки [условия](https://newlang.net/ops.html#условный-оператор) и [циклов](https://newlang.net/ops.html#операторы-циклов). 
- Оператор цикла **while** теперь поддерживает конструкцию [**else**](https://newlang.net/ops.html#операторы-циклов).
- В синтаксис NewLang добавлены [пространства имен](https://newlang.net/syntax.html#пространства-имен).
- Реализована часть концепции ООП и добавлена поддержка [определения классов](https://newlang.net/type_oop.html).
- Переработана идеология [возвратов из функции и обработки исключений](https://newlang.net/newlang_doc.html#операторы-прерывания-выполнения-оператор-возврата).

### Разное
- Выполнен переход на clang 15
- Реализован вызов функций с помощью libffi
- Сделана полноценная поддержка Windows

---

## [Релиз 0.2 (11.08.2022)](https://github.com/rsashka/newlang/releases/tag/v0.2.0)
### Новые возможности и изменения в синтаксисе NewLang
- Добавлены макросы (появилась возможность использовать более привычный синтаксис на основе ключевых слов)
- Реализованы итераторы
- Добавлен новый тип данных - рациональные числа не ограниченной точности
- Многострочные комментарии стали вложенными
- Имена встроенных типов переименованы с указанием размерности

### Другие важные изменения
- Вместо использования gcc перешел на clang, а libffi замененил на JIT компиляцию вызова для нативных функций
- В релиз добавлены бинарные сборки для Linux
- Начало портирования кода на Windows

## [Релиз 0.1 (24.06.2022) - первая публичная версия](https://github.com/rsashka/newlang/releases/tag/v0.1.0)
- Представление общей концепции языка
- Сборка тестов и примеров под Linux из исходников

